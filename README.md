A new generation of vanilla-friendly ships. Simulated sailing, placeable containers, walkable decks -- oh, and cannons!

Modrinth Releases: https://modrinth.com/mod/alekiships/versions

Curseforge Releases: https://www.curseforge.com/minecraft/mc-mods/alekiships/files