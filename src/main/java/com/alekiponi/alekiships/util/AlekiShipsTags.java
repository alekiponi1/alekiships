package com.alekiponi.alekiships.util;

import com.alekiponi.alekiships.AlekiShips;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.structure.Structure;

public final class AlekiShipsTags {


    public static final class Blocks {
        public static final TagKey<Block> PLANTS_THAT_GET_MOWED = create("plants_that_get_mowed");

        public static final TagKey<Block> WOODEN_WATERCRAFT_FRAMES = create("wooden_watercraft_frames");

        private static TagKey<Block> create(final String id) {
            return TagKey.create(Registries.BLOCK, new ResourceLocation(AlekiShips.MOD_ID, id));
        }
    }

    public static final class Items {
        /**
         * Tag for items that are for crafting table compartments
         */
        public static final TagKey<Item> CRAFTING_TABLES = create("crafting_tables");
        /**
         * Tag for items that are for shulker box compartments
         */
        public static final TagKey<Item> SHULKER_BOXES = create("shulker_boxes");
        /**
         * Tag that allows things to go into compartments
         */
        public static final TagKey<Item> CAN_PLACE_IN_COMPARTMENTS = create("can_place_in_compartments");
        /**
         * Tag for items which are consumed for the sloop icebreaker upgrade
         */
        public static final TagKey<Item> ICEBREAKER_UPGRADES = create("icebreaker_upgrades");

        public static TagKey<Item> create(final String id) {
            return TagKey.create(Registries.ITEM, new ResourceLocation(AlekiShips.MOD_ID, id));
        }
    }

    public static final class Entities {
        /**
         * All sloops
         */
        public static final TagKey<EntityType<?>> SLOOPS = create("sloops");
        /**
         * All rowboats
         */
        public static final TagKey<EntityType<?>> ROWBOATS = create("rowboats");
        /**
         * Vehicle helpers such as our collision entities
         */
        public static final TagKey<EntityType<?>> VEHICLE_HELPERS = create("vehicle_helpers");
        /**
         * All compartments
         */
        public static final TagKey<EntityType<?>> COMPARTMENTS = create("compartments");

        public static TagKey<EntityType<?>> create(final String id) {
            return TagKey.create(Registries.ENTITY_TYPE, new ResourceLocation(AlekiShips.MOD_ID, id));
        }
    }

    public static final class Structures {

        public static final TagKey<Structure> UNFINISHED_SLOOP = create("unfinished_sloop");

        public static final TagKey<Structure> UNFINISHED_ROWBOAT = create("unfinished_rowboat");

        public static TagKey<Structure> create(final String id) {
            return TagKey.create(Registries.STRUCTURE, new ResourceLocation(AlekiShips.MOD_ID, id));
        }
    }
}